resource "aws_instance" "ec2_template" {
  ami           = var.ami_name
  instance_type = var.instance_type
  count         = var.instance_count
  key_name      = var.key_name
  tags = {
    Name = var.terraform_instance[count.index]
  }
}

resource "aws_eip" "eip_manager" {
  instance = element(aws_instance.ec2_template.*.id, count.index)
  count    = var.manager_count
  vpc      = true
}

